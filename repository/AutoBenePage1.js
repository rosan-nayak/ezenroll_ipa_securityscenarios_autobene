module.exports = {
    sections: {
        AutoBenePage: {
            selector: 'body',
            elements: {
                AutoBeneLink: {selector: "#rightContent > h3 > a"},
                //LoginID: {locateStrategy: 'xpath',selector: "///*[@id=\"rightContent\"]/div[2]/form/fieldset/div[1]/input"},
                LoginID: {selector: "#rightContent > div.login > form > fieldset > div:nth-child(1) > input[type=\"text\"]"},
                Pwd: {selector: "#rightContent > div.login > form > fieldset > div:nth-child(2) > input[type=\"password\"]:nth-child(2)"},
                LoginButton: {selector: "#rightContent > div.login > form > fieldset > div.submit > input[type=\"submit\"]"},
                AppIDSearch: {selector: "div.searchapp-text input[name='ShowApp']"},
                GoBtn: {selector: "#SelectApplication > div > div:nth-child(1) > button"},
                Errorlockout: {selector: "#rightContent > div:nth-child(1) > div > p"},
                LogOff: {
                    locateStrategy: 'xpath',
                    selector: "//html/body/div[1]/div[1]/div[1]/div[1]/ul/li[4]/a"
                },
                //Errormsg: {selector: "#rightContent > div:nth-child(1) > div > p[contains(text(),' Sorry!  Your login attempt has failed.  Please check that your Username and Password have been entered correctly.'"},
                Errormsg1: {selector: "#rightContent > div:nth-child(1) > div > p"},
                CompanyIDFilter: {locateStrategy: 'xpath',selector: "//ul[@id=\"mainMenu\"]/li[2]/ul/li[7]/a/span[contains(text(), 'Company ID Filter Setup')]"},
                CustomerGroup: {selector: "#mainMenu > li:nth-child(4) > a > span"},
                CustomerGroupEG: {selector: "#mainMenu > li:nth-child(4) > ul > li:nth-child(1) > a > span"},
                CustomerGroupD: {selector: "#mainMenu > li:nth-child(4) > ul > li:nth-child(2) > a > span"},
                CustomerGroupMD: {selector: "#mainMenu > li:nth-child(4) > ul > li:nth-child(3) > a > span"},
                SelectApp: {
                    locateStrategy: 'xpath',
                    selector: "//*[@id=\"selectExtensionForm\"]/div[2]/div/div/button"
                },
                Administration: {locateStrategy: 'xpath', selector: '//*[@id="mainMenu"]/li[2]/a/span'},
                Change_Menu: {selector: "body > div > div.header.group > div.linksContainer > div.clear > ul > li:nth-child(5) > a"},
                GlobalParameters: {selector: "#rightContent > ul:nth-child(3) > li:nth-child(2) > a"},
                eElectRadiobtn: {selector: "#rightContent > form > table > tbody > tr:nth-child(5) > td:nth-child(1) > input[type=\"radio\"]:nth-child(3)"},
                GlobalContinueBtn: {selector: "#submit1"},
                GlobalContinueBtn2: {selector: "#submit1"},
                SecurityRequirement: {selector: "#rightContent > ul:nth-child(5) > li:nth-child(3) > a"},
                ESCheckbox: {selector: "#ckbUsePassSecurity"},
                localComputer: {selector: '#locloc'},
                Existtable: {selector: "#rbExisting"},
                ExitSecTab: {selector: "#sel_SECGROUP_ID > option:nth-child(14)"},
                Continue1: {selector: "#rightContent > form > table:nth-child(6) > tbody > tr > td:nth-child(1) > input[type=\"submit\"]"},
                BeneLink: {selector: "#LinkCenter > a:nth-child(2)"},
                Upload_FileType: {locateStrategy: 'xpath',selector: '//select[@id=\'AutoBeneMainContentPlaceHolder_ddlFileType\']/option[contains(text(), \'Health Benefit Cost\')]'},
                Upload_File: {selector: '#AutoBeneMainContentPlaceHolder_fuCompanyIDFilterFile'},
                Upload_Button: {locateStrategy: 'xpath', selector: '//input[@value=\'Upload\']'},
                ErrorStatus: {selector: '#AutoBeneMainContentPlaceHolder_lblStatus'},
                Error_upload: {locateStrategy: 'xpath', selector: '//span[@id=\'AutoBeneMainContentPlaceHolder_lblStatus\']'}
            }
        },
        BeneDetailsSection: {
            selector: 'body',
            elements: {
                BeneDetailsinput: {selector: "div.wrapper2 input"},
                BeneContinue: {selector: "div.buttonized input"},
                Bene_Username: {selector: "div.desktopmodules_signin_signin_ascx input"},
                Bene_Pwd: {locateStrategy: 'xpath', selector: "//div[@class='desktopmodules_signin_signin_ascx']//div[@class='modulePadding moduleScrollBars']/table/tbody/tr[4]/td/input"},
                Bene_Signin: {locateStrategy: 'xpath', selector: "//div[@class='desktopmodules_signin_signin_ascx']//div[@class='modulePadding moduleScrollBars']/table/tbody/tr[6]/td/input[@type='submit']"},
                AccSettings: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Account Settings')]"},
                Cancelbtn: {selector: "#account-help-cancel-btn"},
                Errormsg: {selector: "body > div > div.create-user-error > div > div.mul-box-bdy > p[contains(text(),'We are sorry, the Password does not match the system security for the Username you entered. Please check the Username and Password combination and enter them again. Note: After 3 failed attempts your account will be locked."},
                Errormsg1: {selector: "body > div > div.create-user-error > div > div.mul-box-bdy > p"},
                BeneSearch: {selector: "#portalIdBox"},
                GoContinue: {selector: "#goButton"},
                Errorlockout: {selector: '#rightContent > div:nth-child(1) > div > p'},
                LogOff: {selector: '#ctl03_Banner1_PortalHeaderMenu > tbody > tr > td:nth-child(3) > a'},
                AdminThis: {selector: '#ctl03_Banner1_PortalTabs > tbody > tr > td:nth-child(11) > a'},
                LogoFile: {selector: 'ctl03$Desktopthreepanes2$ThreePanes$ctl11$SITESETTINGS_LOGO'},
                browse_file: {selector: '#ctl03_Desktopthreepanes2_ThreePanes_ctl11_EditTable > fieldset.SettingsTableGroup.theme_layout_settings > table > tbody > tr:nth-child(3) > td.st-control > input[type="submit"]:nth-child(2)'},
                localComputer: {selector: '#locloc'},
                Upload_FileType: {selector: '#AutoBeneMainContentPlaceHolder_ddlFileType > option:nth-child(3)'},
                Upload_File: {selector: '#AutoBeneMainContentPlaceHolder_fuCompanyIDFilterFile'},
            }
        }
    }
};