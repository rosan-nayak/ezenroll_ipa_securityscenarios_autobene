
var seleniumServer = require('selenium-server');
var chromedriver = require('chromedriver');
var data1 = require('./test_resources/Security_Data.js');
require('nightwatch-cucumber')({
    supportFiles: ['./utils/TestExecListener.js'],
    stepTimeout:3000000,
    defaultTimeoutInterval:900000,
   // nightwatchClientAsParameter:true,
})

module.exports = {
  output_folder: 'reports',
  custom_commands_path: '',
  custom_assertions_path: '',
  page_objects_path : 'repository',
  live_output: false,
  disable_colors: false,
  selenium: {
    start_process: true,
    server_path: seleniumServer.path,
    host: '127.0.0.1',
      port: 5555,
    cli_args: {
        'webdriver.chrome.driver': chromedriver.path,
        'webdriver.ie.driver': 'Z:\\IEDriverServer.exe',
        'webdriver.firefox.profile': 'MMC_User_48',
        'webdriver.gecko.driver': 'C:\\US_BEN_Security_Testing\\node_modules\\geckodriver.exe',
    }
  },

  test_settings: {
    default : {
      launch_url: "http://localhost",
      page_objects_path : 'repository',
      selenium_host: "127.0.0.1",
        selenium_port: 5555,
      silent : true,
      disable_colors: false,
        screenshots: {
            enabled: true,
            on_failure: true,
            on_error: true,
            path: 'Screenshots'
        },

        desiredCapabilities: {
            browserName: data1.BrowserInTest,
        javascriptEnabled : true,
        acceptSslCerts : true
      }
    },
  }
}

