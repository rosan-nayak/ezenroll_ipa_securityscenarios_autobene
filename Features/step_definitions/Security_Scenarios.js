var data1 = require('../../test_resources/Security_Data');
//var action1 = require('../../utils/CommonUtilityEZEnroll');
var objects = require('../../repository/AutoBenePage1');
//var action = require('./../../Keywords');
//var clipboard = require('../../node_modules/copy-paste');
var clipboard = require('copy-paste');
var AutoBenePage, BeneDetailsSection, Pages;


var execEnv = data1["TestingEnvironment"];


function initializePageObjects(browser, callback) {

    Pages = browser.page.AutoBenePage1();
    AutoBenePage = Pages.section.AutoBenePage;
    BeneDetailsSection = Pages.section.BeneDetailsSection;
    callback();
}


module.exports = function () {
    //Feature : Authorization Testing
    // TagName : BeneDetails
    //Scenario : Testing for Privilege escalation

    this.Given(/^Employee opens BeneDetails Application in Browser$/, function () {
        var BeneURL;
        browser = this;
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "TEST1" || execEnv.toUpperCase() == "QA" || execEnv.toUpperCase() == "STAGING" || execEnv.toUpperCase() == "PROD") {
            BeneURL = data1.urls.BENE_DETAILS[execEnv].vaildUrl;

            browser.maximizeWindow()
            // .deleteCookies()
                .url(BeneURL);
            browser.timeoutsImplicitWait(data1.longWait);
            initializePageObjects(browser, function () {
                BeneDetailsSection.waitForElementVisible('@BeneDetailsinput', data1.shortWait);
                BeneDetailsSection.setValue('@BeneDetailsinput', data1.users.BENE_DETAILS[execEnv].ApplicationID);
                BeneDetailsSection.click('@BeneContinue', function () {
                });
            })
        }
    });


    this.When(/^Employee enters Username and Password in Login Page$/, function () {
        browser.timeoutsImplicitWait(data1.shortWait);
        BeneDetailsSection.click('@Bene_Username');
        browser.getCookie('@Bene_Username', function (result) {
            console.log(result);
        });
        BeneDetailsSection.setValue('@Bene_Username', data1.users.BENE_DETAILS[execEnv].username);                 // Enter Username
        BeneDetailsSection.setValue('@Bene_Pwd', data1.users.BENE_DETAILS[execEnv].password);                      // Enter password
        browser.pause(2000);
        BeneDetailsSection.click('@Bene_Signin', function () {
        });

    });
    this.When(/^Employee try to edit Benedetails Information$/, function () {
        browser.timeoutsImplicitWait(data1.shortWait);
        BeneDetailsSection.waitForElementNotPresent('@AccSettings', data1.shortWait);
        browser.pause(data1.shortWait);
    });


    //Feature : Authorization Testing
    //TagName : AutoBene
    //Testing for Bypassing Authorization Schema
    this.Given(/^Employee opens AutoBene application in browser$/, function () {
        var URL;
        browser = this;
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "TEST1" || execEnv.toUpperCase() == "QA" || execEnv.toUpperCase() == "STAGING" || execEnv.toUpperCase() == "PROD") {
            URL = data1.urls.AUTO_BENE[execEnv].vaildUrl;
            initializePageObjects(browser, function () {

                browser.maximizeWindow()
                    .url(URL);

                browser.pause(data1.shortWait);
                browser.frame(1, function () {
                    AutoBenePage.click('@AutoBeneLink')
                });
            })
        }
    });

    this.Given(/^Employee enters login details in Login Page$/, function () {
        browser.frame(1, function () {
            browser.pause(data1.shortWait);
            AutoBenePage.setValue('@LoginID', data1.users.AUTO_BENE[execEnv].username, function () {
                AutoBenePage.setValue('@Pwd', data1.users.AUTO_BENE[execEnv].password, function () {
                    AutoBenePage.click('@LoginButton', function () {
                        browser.pause(data1.longWait);
                    });
                });
            });
        });
    });

    this.When(/^Employee enters AppID$/, function () {
        browser.timeoutsImplicitWait(2000);
        browser.frame(1, function () {
            browser.pause(data1.shortWait);

            AutoBenePage.setValue('@AppIDSearch', data1.users.AUTO_BENE[execEnv].ApplicationID, function () {
                AutoBenePage.click('@GoBtn', function () {
                });
            });
            browser.pause(data1.shortWait);
        })
    });

    this.When(/^Verify Customer Groups doesn't display Master Distributor or Distributor$/, function () {
        browser.timeoutsImplicitWait(2000);
        AutoBenePage.click('@Change_Menu', function () {
        });
        browser.pause(data1.shortWait);
        AutoBenePage.click('@CustomerGroup', function () {
        });
        browser.pause(data1.shortWait);
        AutoBenePage.waitForElementNotPresent('@CustomerGroupD', data1.shortWait);
        AutoBenePage.waitForElementNotPresent('@CustomerGroupMD', data1.shortWait);
        AutoBenePage.waitForElementVisible('@CustomerGroupEG', data1.shortWait);
        browser.pause(data1.shortWait);
        AutoBenePage.click('@CustomerGroupEG', function () {
        });
    });

    this.When(/^Employee selects eElect option for Application used for in Global Parameters Page do not select BeneDetails option$/, function () {
        browser.timeoutsImplicitWait(2000);
        AutoBenePage.click('@Change_Menu', function () {

            browser.pause(data1.shortWait);
            AutoBenePage.click('@GlobalParameters', function () {

                browser.pause(data1.shortWait);
                AutoBenePage.click('@eElectRadiobtn', function () {

                    browser.pause(3000);
                    AutoBenePage.click('@GlobalContinueBtn', function () {

                        browser.pause(data1.shortWait);
                        AutoBenePage.click('@GlobalContinueBtn2', function () {
                        });
                    });
                });
            });
        });
    });


    //Feature : Authentication Testing
    //TagName :
    //Testing for Bypassing Authorization Schema
    this.Given(/^Validate Testenvironment URL$/, function () {
        var invalidURL;
        browser = this;
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "TEST1" || execEnv.toUpperCase() == "QA" || execEnv.toUpperCase() == "STAGING" || execEnv.toUpperCase() == "PROD") {

            invalidURL = data1.urls.AUTO_BENE[execEnv].invalidUrl;
            var validURL = data1.urls.AUTO_BENE[execEnv].vaildUrl;
            console.log(invalidURL);
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .pause(2000)
                    .url(invalidURL);
                browser.url(function (response) {
                    console.log();
                    if (response.value == invalidURL) {
                        browser.assert.equal(response.value, validURL);
                        console.log("Invalid URL");
                    }
                })
            })
        }
    });

    //Feature : Authentication Testing
    //TagName : HTTP
    //Testing for Testing for default credentials

    this.When(/^Employee enters Enhances Security App#$/, function () {
        browser.timeoutsImplicitWait(2000);
        browser.frame(1, function () {
            browser.pause(data1.shortWait);
            AutoBenePage.setValue('@AppIDSearch', data1.users.AUTO_BENE[execEnv].Es_AppID)
            AutoBenePage.click('@GoBtn')
        })
    });

    this.When(/^Enable Enhanced Security in AutoBene$/, function () {
        browser.timeoutsImplicitWait(2000);
        browser.frame(1, function () {
            browser.pause(data1.shortWait);
            AutoBenePage.click('@Change_Menu', function () {
            });
            browser.pause(2000);
            AutoBenePage.click('@SecurityRequirement', function () {
            });

            //  browser.verify.visible('input[name="ckbUsePassSecurity"]', 'The checkbox named vote is visible')
            //      .waitForElementVisible('body', 1000)
            //      .element('name', 'ckbUsePassSecurity', function (response) {
            //          browser.elementIdSelected(response.value.ELEMENT, function (result) {
            //              browser.verify.ok(result.value); //'Checkbox is selected');
            //              console.log(result.value);
            //              browser.pause(2000);
            browser.pause(data1.shortWait);
            AutoBenePage.click('@Continue1', function () {
            });
            browser.pause(data1.shortWait);
            AutoBenePage.click('@BeneLink', function () {
            });
            browser.pause(data1.shortWait);
            browser.windowHandles(function (result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.maximizeWindow();
                browser.pause(data1.shortWait);
                BeneDetailsSection.setValue('@BeneSearch', data1.users.BENE_DETAILS[execEnv].Es_AppID);
                BeneDetailsSection.click('@GoContinue', function () {
                });
            });

        });
    });

    this.When(/^Try to Login BeneDetails through EID and PIN$/, function () {
        browser.timeoutsImplicitWait(data1.shortWait);
        BeneDetailsSection.setValue('@Bene_Username', data1.users.BENE_DETAILS[execEnv].EID, function () {

        });
        browser.pause(data1.shortWait);                                    // Enter Username
        BeneDetailsSection.setValue('@Bene_Pwd', data1.users.BENE_DETAILS[execEnv].PIN);             // Enter password
        browser.pause(data1.shortWait);
        BeneDetailsSection.click('@Bene_Signin', function () {
        });
        browser.pause(data1.shortWait);
    });


    //Feature : Error Handling
    //TagName : Error Code
    //Testing for Error Code

    this.When(/^Employee enters incorrect Username and Password$/, function () {
        browser.pause(data1.shortWait);
        AutoBenePage.setValue('@LoginID', data1.users.AUTO_BENE[execEnv].invalidUsername, function () {

            // Enter Username
            AutoBenePage.setValue('@Pwd', data1.users.AUTO_BENE[execEnv].invalidpassword, function () {

                // Enter password
                browser.pause(data1.shortWait);
                AutoBenePage.click('@LoginButton')//, function () {
                browser.pause(data1.longWait);
            });
        });
    });
    //});

    this.When(/^Validate Error message$/, function () {
        browser.frame(0, function () {
            AutoBenePage.getText('@Errormsg1', function (response) {
                var expected = "Sorry! Your login attempt has failed. Please check that your Username and Password have been entered correctly.";
                console.log("Excepted: " + expected);
                console.log("Actual: " + response.value);
                browser.assert.equal(response.value, expected);
                if (response.value != expected) {
                    console.log("Text Content mismatch: ");
                }
            });

        })
    });

    this.When(/^Validate the AutoBene application locks out after #mins of idleness$/, function () {
        browser.pause(1800000);
        //browser.refresh();
        AutoBenePage.getText('@Errorlockout', function (text) {
            var Expected_Text = "Your session has timed out. Please login and try again. (0cc) <> ";
            var actual_text = text.value;
            browser.assert.equal(actual_text, Expected_Text);
        })
    });

    this.When(/^Verify browser should auto populate the password of your login.$/, function () {
        AutoBenePage.click('@LogOff', function () {
        });
        //   browser.pause(data1.shortWait);
        //   BeneDetailsSection.getText('@Bene_Username', function (text) {
        //       console.log(text)
        //   });
        //   BeneDetailsSection.getText('@Bene_Pwd', function (text) {
        //       console.log(text)
        //   })
    });
    this.When(/^Verify Cache Weakness$/, function () {

        BeneDetailsSection.click('@LogOff', function () {
        });
        browser.pause(data1.shortWait);
        browser.back();
        browser.pause(data1.shortWait);
        BeneDetailsSection.waitForElementVisible('@Bene_Username', data1.shortWait);
        BeneDetailsSection.getText('@Bene_Username', function (text) {
            var text1 = JSON.stringify(text);
            var arr = text1.split(',');
            var value = arr[2];
            var arr2 = value.split(':');
            var actual_value = arr2[1];
            if (actual_value === "" && typeof variable === "string") ;
            //console.log(actual_value);
            // var expected_value = "";
            // browser.assert.equal(actual_value, expected_value);
        });
        BeneDetailsSection.getText('@Bene_Pwd', function (text) {
            console.log(text);
        });


    });
    this.When(/^Verify Cache Weakness Exist or not$/, function () {

        //AutoBenePage.click('@LogOff', function () {
        //});
        browser.pause(data1.shortWait);
        browser.back();
        browser.pause(data1.shortWait);
        browser.frame(1, function () {
            AutoBenePage.waitForElementVisible('@LoginID', data1.shortWait);
            AutoBenePage.getText('@LoginID', function (text) {
                var actual_value = text.value;
                var expected_value = "";
                browser.assert.equal(actual_value, expected_value);
            });
            AutoBenePage.getText('@Pwd', function (text) {
                var actual_value_pwd = text.value;
                var expected_value_pwd = "";
                browser.assert.equal(actual_value_pwd, expected_value_pwd);
                browser.end()
            });
        });
    });


    this.When(/^Test Upload of Unexpected File Types$/, function () {
        AutoBenePage.click('@Change_Menu', function () {
            browser.pause(data1.shortWait);
            AutoBenePage.click('@Administration', function () {
                browser.pause(data1.shortWait);

                AutoBenePage.click('@CompanyIDFilter', function () {

                    browser.pause(data1.shortWait);

                })
            })
        })
    })

    this.When(/^Validate the "([^"]*)" unexpected File type error message "([^"]*)"$/, function (filePath, fileName) {

        browser.pause(data1.shortWait);
        AutoBenePage.click('@Upload_FileType', function () {
            browser.pause(data1.shortWait);
            browser.setValue('input[type=\'file\']', require('path').resolve(filePath), function () {
                browser.pause(data1.shortWait);

                AutoBenePage.click('@Upload_Button', function () {
                    browser.pause(data1.longWait);
                    var expected_text = "Error uploading file: Error Upload failed for " + fileName + "";
                    AutoBenePage.getText('@Error_upload', function (text1) {
                        //var text1 = JSON.stringify(text);
                        //console.log(text1);
                        var actual_text = text1.value;
                        browser.assert.equal(actual_text, expected_text)
                    })
                })
            })
        })
    })

    // })
    //})
}


