
@AutoBene_Security

Feature: Authorization Testing

  @PrivilegeCGTest
  Scenario: Testing for Privilege escalation
    Given Employee opens AutoBene application in browser
    When  Employee enters login details in Login Page
    When  Employee enters AppID
    Then  Verify Customer Groups doesn't display Master Distributor or Distributor


    @AuthorizationABTest
    Scenario: Testing for Bypassing Authorization Schema
     Given Employee opens AutoBene application in browser
      When Employee enters login details in Login Page
     When  Employee enters AppID
     And   Employee selects eElect option for Application used for in Global Parameters Page do not select BeneDetails option
