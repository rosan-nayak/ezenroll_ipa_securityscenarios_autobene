@AutoBene_Security
Feature: Business Logic Testing
  @UnexpectedFileABTest
  Scenario Outline: Test Upload of Unexpected File Type
    Given Employee opens AutoBene application in browser
    When  Employee enters login details in Login Page
    When  Employee enters AppID
    Then Test Upload of Unexpected File Types
    Then Validate the "<filePath>" unexpected File type error message "<fileName>"

    Examples:
      | filePath                 | fileName  |
      | test_resources/Test.app  | Test.app  |
      | test_resources/Test.bash | Test.bash |
      | test_resources/Test.bat  | Test.bat  |
      | test_resources/Test.cmd  | Test.cmd  |
      | test_resources/Test.dll  | Test.dll  |
      | test_resources/Test.exe  | Test.exe  |
      | test_resources/Test.sql  | Test.sql  |
      | test_resources/Test.bin  | Test.bin  |

