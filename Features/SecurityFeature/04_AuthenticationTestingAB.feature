@AutoBene_Security
Feature: Authentication Testing

  #@EncryptedChannel
 # Scenario: Credentials transport over an encrypted channel
   # Given Employee opens AutoBene application in browser
   # When  When Employee enters login details in Login Page
    # Then  Verify the Username and Password of AutoBene user is passed in an encrypted manner


  #@EnhancedSecurity
  #Scenario: Testing for default credentials
   # Given Employee opens AutoBene application in browser
   # When Employee enters login details in Login Page
  #  And Employee enters Enhances Security App#
   # When  Enable Enhanced Security in AutoBene
  #  Then  Try to Login BeneDetails through EID and PIN

  @HTTPABTest
  Scenario: HTTP Strict Transport Security
    Given Validate Testenvironment URL


  @LockOutABTest
  Scenario: Testing for Weak lock out mechanism
    Given Employee opens AutoBene application in browser
    When  Employee enters login details in Login Page
    Then Validate the AutoBene application locks out after #mins of idleness

 # @Remember
  #Scenario: Testing for Vulnerable remember password
   # Given Employee opens AutoBene application in browser
   # When  Employee enters login details in Login Page
   # And   Verify browser should auto populate the password of your login.

  @CacheABTest
  Scenario: Testing for Browser cache weakness
    Given Employee opens AutoBene application in browser
    When  Employee enters login details in Login Page
    Then Verify Cache Weakness Exist or not












