//console.log(process.env.TEST_ENV);
module.exports = {
    BrowserInTest: 'chrome',
    //TestingEnvironment: process.env.TEST_ENV,
    TestingEnvironment: 'Test1',
    shortWait: 2000,
    longWait: 20000,


    urls: {
        AUTO_BENE: {
            Test1: {
                vaildUrl: 'https://test1.autobene.com/',
                invalidUrl: 'http://test1.autobene.com'
            },

            QA: {
                vaildUrl: 'https://test2.autobene.com/',
                invalidUrl: 'http://test2.autobene.com'

            },
            Staging: {
                vaildUrl: 'https://test3.autobene.com/',
                invalidUrl: 'http://test3.autobene.com'
            },
            Prod: {
                vaildUrl: 'https://www.autobene.com/',
                invalidUrl: 'http://www.autobene.com'

            }
        },
        BENE_DETAILS: {
            Test1: {
                vaildUrl: 'https://test.benedetails.com/',
                invalidUrl: 'http://test.benedetails.com/'
            },

            QA: {
                vaildUrl: 'https://test2.benedetails.com/',
                invalidUrl: 'http://test2.benedetails.com/'

            },
            Staging: {
                vaildUrl: 'https://test3.benedetails.com/',
                invalidUrl: 'http://test3.benedetails.com/'
            },
            Prod: {
                vaildUrl: 'https://www.benedetails.com/',
                invalidUrl: 'http://www.benedetails.com/'

            }

        }

    },
    users: {
        AUTO_BENE: {
            Test1: {
                username: 'Automation6834',
                password: 'Welcome6',
                ApplicationID: '89624',
                invalidUsername: 'Automation68345',
                invalidpassword: 'Welcome6',
                Es_AppID: '100101'
            },
            QA: {
                username: 'Automation6834',
                password: 'Welcome6',
                invalidUsername: 'Automation68345',
                invalidpassword: 'Welcome6',
                ApplicationID: '91288'
            },
            Staging: {
                username: 'Automation6834',
                password: 'Welcome6',
                invalidUsername: 'Automation68345',
                invalidpassword: 'Welcome6',
                ApplicationID: '96033'

            },
            Prod: {
                username: 'Automation6834',
                password: 'Welcome6',
                invalidUsername: 'Automation68345',
                invalidpassword: 'Welcome6',
                ApplicationID: '96051'
            }
        },
        BENE_DETAILS: {
            Test1: {
                username: 'Automation6834',
                password: 'Welcome5',
                ApplicationID: '89622',
                invalidUsername: 'bharathiav',
                invalidpassword: 'silver11',
                Es_AppID: "100101",
                EID: '111114',
                PIN: '11111'
            },
            QA: {
                username: 'Automation6834',
                password: 'Welcome5',
                invalidUsername: 'bharathiav',
                invalidpassword: 'silver11',
                ApplicationID: '91288'
            },
            Staging: {
                username: 'BharathiAv',
                password: 'Silver11',
                invalidUsername: 'bharathiav',
                invalidpassword: 'silver11',
                ApplicationID: '96033'

            },
            Prod: {
                username: 'BharathiAv',
                password: 'Silver11',
                invalidUsername: 'bharathiav',
                invalidpassword: 'silver11',
                ApplicationID: '96051'

            }
        }
    },
    SectionData: {}
};